package my.tdl.gamestate;

import java.awt.Graphics2D;

import my.project.gop.main.SpriteSheet;
import my.project.gop.main.loadImageFrom;
import my.tdl.generator.Map;
import my.tdl.main.Main;

public class DungeonLevelLoader extends GameState{
	//This line is creating a new map
	Map map;

	public DungeonLevelLoader(GameStateManager gsm) {
		super(gsm);
	}
	//This is initializing the map
	@Override
	public void init() {
		 map = new Map();
		 map.init();
	}
	//This is ticking the map
	//NOTE: Ask what ticking is
	@Override
	public void tick(double deltaTime) {
		map.tick(deltaTime);
	}
	//This renders the map
	@Override
	public void render(Graphics2D g) {
		map.render(g);
		
	}

}
