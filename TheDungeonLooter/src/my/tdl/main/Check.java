package my.tdl.main;

import java.awt.Point;

import my.tdl.generator.Block;
import my.tdl.generator.TileManager;

public class Check {
	
	public static boolean CollisionPlayerBlock(Point p1, Point p2){
		//This line loops through all the blocks
		for(Block block : TileManager.blocks){
			//this determines if the blocks are solid
			if(block.isSolid()){
				//this checks to see if that block contains these to points and if it does the it will register it as colliding
				if(block.contains(p1) || block.contains(p2)){
					return true;
				}
			}
		}
		return false;
	}
}