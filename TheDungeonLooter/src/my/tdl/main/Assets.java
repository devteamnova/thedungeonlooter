package my.tdl.main;

import java.awt.image.BufferedImage;

import my.project.gop.main.SpriteSheet;
import my.project.gop.main.loadImageFrom;

public class Assets {
	
	SpriteSheet blocks = new SpriteSheet();
	
	public static BufferedImage stone_1;
	public static BufferedImage stone_2;
	
	public void init(){
		blocks.setSpriteSheet(loadImageFrom.LoadImageFrom(Main.class, "spritesheet.png"));
		//This is telling the program to grab the image at location of 0,0 and for it to be 16 by 16 pixels
		stone_1 = blocks.getTile(0, 0, 16, 16);
		stone_2 = blocks.getTile(0, 16, 16, 16);
		
	}
	
	
	public static BufferedImage getStone_1() {
		return stone_1;
	}
	
	public static BufferedImage getStone_2() {
		return stone_2;
	}


}
