package my.tdl.generator;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import my.project.gop.main.Vector2F;
import my.project.gop.main.loadImageFrom;
import my.tdl.MoveableObjects.Player;
import my.tdl.generator.Block.BlockType;
import my.tdl.main.Main;

public class Map {
	//This handles all of the blocks by doing various things such as updating them
	TileManager tiles = new TileManager();
	Player player = new Player();

	public Map() {
		
	}
	
	public void init(){
		player.init();
		//this is creating a new Buffered image as the map
		BufferedImage map = null;

		try {
			map = loadImageFrom.LoadImageFrom(Main.class, "Map.png");
		}catch(Exception e){
			
		} 
		
		for(int x = 0; x < 100;x++){
			for(int y = 0; y < 100;y++){
				//This is taking that RGB from all of the pixels from them map and putting them into the value col(which is shot for colors)
				int col = map.getRGB(x, y);
				
				//This is telling it to switch from col to 0xFFFFFF
				switch(col & 0xFFFFFF){
				//This then checks col to see if any of the colors are 0x808080
				case 0x808080:
					//This takes the blocks that were 0x808080 and puts a tile in their place
					tiles.blocks.add(new Block(new Vector2F(x*48, y*48), 	BlockType.STONE_1).isSolid(true));
				break;
				case 0xCCCCCC:
					tiles.blocks.add(new Block(new Vector2F(x*48, y*48), 	BlockType.STONE_2));
				break;
				
				}
				
			}
		}
		
	}
	//This ticks the tiles
	public void tick(double deltaTime){
		tiles.tick(deltaTime);
		player.tick(deltaTime);
	}
	//This renders the tiles
	public void render(Graphics2D g){
		tiles.render(g);
		player.render(g);
		
	}

}
