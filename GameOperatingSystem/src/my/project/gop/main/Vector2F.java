package my.project.gop.main;

public class Vector2F {
	//Creating two new floats for xpos ypos
	public float xpos;
	public float ypos;
	//Location at which the world will be drawn
	public static float worldXpos;
	public static float worldYpos;
	//Here we are creating it as zero
	//If we call a normal constructor it is zeroed out
	public Vector2F() {
		this.xpos = 0.0f;
		this.ypos = 0.0f;
	}
	//Here it is being created
	//If passing  these two floats, they set the two variables on 5 and 6	
	public Vector2F(float xpos, float ypos) {
		this.xpos = xpos;
		this.ypos = ypos;
	}
	//This is setting it to zero
	//Zeroing method zeroing it to zero
	public static Vector2F zero() {
		return new Vector2F(0,0);
	}
	//This is normalizing it
	public void normalize(){
		double length = Math.sqrt(xpos * xpos + ypos * ypos);
		
		if(length != 0.0){
			float s = 1.0f / (float) length;
			xpos = xpos*s;
			ypos = ypos*s;
		}
	}
	//This puts the vector on the screen
	//This gets the screen location
	public Vector2F getScreenLocation(){
		return new Vector2F(xpos, ypos);
	}
	//This puts the vector into the world
	//This gets the world location
	public Vector2F getWorldLocation(){
		return new Vector2F(xpos - worldXpos, ypos - worldYpos);
		//QUESTIONS TO ASK: Why are we subtracting xpos from worldxpos
		//NOTE: In worldXpos and WorldYpos remember the Y and X are capitalized
	}
	
	//Checks to see if  whether or not the vectors that you are checking with are equal
	//This can equalize vectors
	public boolean equals(Vector2F vec){
		return (this.xpos == vec.xpos && this.ypos == vec.ypos);
	}
	// Copies and adds vectors
	//This makes it so you can copy a vector
	public Vector2F copy(Vector2F vec){
		xpos = vec.xpos;
		ypos = vec.ypos;
		return new Vector2F(xpos, ypos);
	}
	//This makes it so you can add a vector
	public Vector2F add(Vector2F vec){
		xpos = xpos + vec.xpos;
		ypos = ypos + vec.ypos;
		return new Vector2F(xpos, ypos);
	}
	
	public static void setWorldVariables(float wx, float wy){
		worldXpos = wx;
		worldYpos = wy;
	}
	//This finds the distance between two vectors
	//The the distance in the screen between two HUDs
	public static double getDistanceOnScreen(Vector2F vec1, Vector2F vec2){	
		float v1 = vec1.xpos - vec2.xpos;
		float v2 = vec1.ypos - vec2.ypos;
		return Math.sqrt(v1*v1 + v2*v2);
	}
	//This find the distance between to world objects like a player nd a chest
	public double getThisStanceBetweenWorldVectors(Vector2F vec){
		
		float dx = Math.abs(getWorldLocation().xpos - vec.getWorldLocation().xpos);
		float dy = Math.abs(getWorldLocation().ypos - vec.getWorldLocation().ypos);
		return Math.abs(dx * dx - dy * dy);
	}
}
